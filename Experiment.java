import java.util.Scanner;

public class Experiment {
    /*** In fibonacci series, next number is the sum of previous two numbers
    for example 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 etc. The first two numbers of fibonacci series are 0 and 1.
    with recursion and without recursion ***/

    static void fibCalc(int n) {
        int sum = 0, n1 = 0, n2 = 1, n3 = 0;

        if (n < 1) {
            System.out.println("Please enter a valid number");
        }
        else if (n == 1) {
            System.out.println("Print the numbers: "+"\n"+ n1);
            sum = n1;
        }
        else if (n == 2) {
            sum = n2;
            System.out.println("Print the numbers: "+"\n"+n1+"\n"+n2);
        }
        else {
            System.out.println("Print the numbers: "+"\n"+n1+"\n"+n2);
            for (int i = 3; i <= n; i++) {
                n3 = n1 + n2;
                n1 = n2;
                n2 = n3;
                System.out.println(n3);
                sum = sum + n3;
              }
        }
        System.out.println("Print the sum of numbers: " + sum);
    }


    public static void main(String[] args) {
        System.out.println("Enter the number: ");
        Scanner sc= new Scanner(System.in);
        int n= sc.nextInt();
        fibCalc(n);
    }
}
